#!/bin/sh
LOGFILE=/var/log/ovpn-debug.log 
DATE=`/bin/date` 
echo "**** $DATE **** learn-script/BEGIN ****" >> $LOGFILE
echo $DATE $1 $2 $3 >> $LOGFILE

# learn-address script environment dump
/usr/bin/env >> $LOGFILE
echo "**** $DATE **** learn-script/END ****" >> $LOGFILE
