#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Requires:
# python3-ldap3 ver 2.2.4[tested]

# Скрипт для настройки IPTABLES согласно членству в группах в домене
# Скрипт имеет 3 режима работы, согласно документации OpenVPN,
# 'add' - добавление IP-адреса, происходит при подключении,
# 'update' - обновление, при изменении IP-адреса у клиента,
# 'delete' - удаление IP-адреса, при отключении.
# В конфигурационном файле openvpn-acl.conf настраиваются группы AD и соотвтествующие им списки сетей IPSET,
# а также другие параметры

import os
import sys
import ldap3
import importlib.util                                      
import importlib.machinery                                                  
import logging
from subprocess import run
logging.basicConfig(format='%(levelname)s: %(asctime)s %(message)s',datefmt='%m/%d/%Y %H:%M:%S',level=logging.DEBUG)
#logging.basicConfig(filename='ovpn-init.log',format='%(levelname)s: %(asctime)s %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p',level=logging.DEBUG)

try:
    loader = importlib.machinery.SourceFileLoader('cfg', '/etc/openvpn/scripts/learn_address.conf')
    spec = importlib.util.spec_from_loader(loader.name, loader)
    conf = importlib.util.module_from_spec(spec)
    loader.exec_module(conf)
except IOError:
    logging.warning('* Error loading config file')
    sys.exit(1)

def ldapinit(uri, username, passwd):
    ''' Функция предназначена для подключения к серверу AD'''
    try:
        server = ldap3.Server(uri, get_info=ldap3.ALL)
        ad = ldap3.Connection(server, username, passwd, auto_bind=True)
    except Exception as error:
        logging.warning('* Error connecting to LDAP: {}'.format(error))
    return ad

def get_user_attrs(user):
    ''' Функция ищет указанного в параметре пользователя в AD,
    если пользователь найден, то возвращает атрибуты в словаре с ключами:
    'sAMAccountName' - может совпадать с параметром <user>, за исключением регистра
    'cn' - каноническое имя пользователя в AD, например, "CN=user,OU=Groups,DC=example,DC=domain,DC=local"
    'memberOf' - содержит все группы, в которых пользователь имеет членство
    '''
    ad_conn = ldapinit(conf.ldapuri, conf.user, conf.pw)
    search_filter = '(&(sAMAccountName={}))'.format(user)
    retrieve_attributes = ['sAMAccountName', 'cn', 'memberOf']
    ad_conn.search(conf.base_dn, search_filter, attributes=retrieve_attributes)
    user_attributes = ad_conn.entries
    ad_conn.unbind()
    try:
        attr = user_attributes[0].entry_attributes_as_dict
        logging.info('get_user_attr: {}, attributes: {}'.format(user, attr))
        return attr
    except:
        logging.error('get_user_attr: Requested user: {} not found in LDAP'.format(user))
        return 'Not Found'

def get_ovpn_group_list(ipset_group):
    '''
    Функция возвращает список групп, которые содержат сети в атрибуте 'info'
    на основе этих данных будут строится правила для IPTables
    '''
    ad_conn = ldapinit(conf.ldapuri, conf.user, conf.pw)
    search_filter = '(memberOf={})'.format(ipset_group)
    retrieve_attributes = [conf.ipset_name_attr, conf.ipset_iplist_attr]
    ad_conn.search(conf.base_dn, search_filter, attributes=retrieve_attributes)
    groups = ad_conn.entries
    ad_conn.unbind()
    for group in groups:
        logging.info('get_ovpn_group_list: {}, attributes: {}'.format(group.entry_dn, group.entry_attributes_as_dict))
    return groups


def run_command(stage, cmd_descr, command):
    logging.info('{}: Running: {}'.format(stage, command))
    try:
        run(command, check=True)
    except Exception as error:
        logging.warning('{}: Failed: {}. Debug: {}'.format(stage, cmd_descr, error))
        return 1


def authorize_user(trusted_ip, common_name):
    stage = 'authorize_user'
    logging.info('Processing ACLs for sAMAccountName:{} with client IP address: {}'.format(common_name, trusted_ip))
    user = get_user_attrs(common_name)
    ipset_groups = get_ovpn_group_list(conf.ipset_group)
    if user == 'Not Found' or len(ipset_groups) == 0:
        return 1
    for acl_group in ipset_groups:
        group_dn = acl_group.entry_dn
        if group_dn in user['memberOf']:
            acl_basename = acl_group.entry_attributes_as_dict[conf.ipset_name_attr][0].replace(' ','_')
            client_ipset_name = acl_basename + conf.client_ipset_suffix
            dest_ipset_name = acl_basename + conf.dest_ipset_suffix
            acl_chain_name = conf.nf_chain_prefix + acl_basename
            dst_iplist = acl_group.entry_attributes_as_dict[conf.ipset_iplist_attr][0].split(';')
            logging.info('{}: Found ACL for LDAP group: {} IP address: {} was added to IPSet: {}!'.format(stage, group_dn, trusted_ip, client_ipset_name))
            apply_acls(acl_basename, dst_iplist, user['sAMAccountName'][0], trusted_ip)
        else:
            logging.info('{}: "{}" have no ACL specified for LDAP group: {} SKIPPING...'.format(stage, common_name, group_dn))
    # Если пользователь состоит в группе, разрешающей MQSUERADE то добавляем его в соответствующий ipset
    if conf.masq_needed in user['memberOf']:
        cmd_key = 'add_user_ip'
        command = conf.acl_commands[cmd_key].format(src_ipset='masquerade_allowed', user_ip=trusted_ip, comment=common_name)
        run_command(stage, cmd_key, command.split())
 
    return 0



def apply_acls(name, dst_iplist, comment, cl_ip):
    stage = 'apply_acls'
    
    cmd_key = 'create_src_ipset'
    command = conf.acl_commands[cmd_key].format(src_ipset=name+conf.client_ipset_suffix)
    run_command(stage, cmd_key, command.split())
    
    cmd_key = 'add_user_ip'
    command = conf.acl_commands[cmd_key].format(src_ipset=name+conf.client_ipset_suffix, user_ip=cl_ip, comment=comment)
    run_command(stage, cmd_key, command.split())
    
    cmd_key = 'create_dst_ipset'
    command = conf.acl_commands[cmd_key].format(dst_ipset=name+conf.dest_ipset_suffix)
    run_command(stage, cmd_key, command.split())
       
    cmd_key = 'flush_dst_ipset'
    command = conf.acl_commands[cmd_key].format(dst_ipset=name+conf.dest_ipset_suffix)
    run_command(stage, cmd_key, command.split())
    
    cmd_key = 'fill_dst_ipset'
    for address in dst_iplist:
        command = conf.acl_commands[cmd_key].format(dst_ipset=name+conf.dest_ipset_suffix, dst_ip=address)
        run_command(stage, cmd_key, command.split())
       
#    cmd_key = 'create_acl_chain'
#    command = conf.acl_commands[cmd_key].format(acl_chain=conf.nf_chain_prefix+name)
#    fatal_error = run_command(stage, cmd_key, command.split())
#    if fatal_error: return 1
    
#    cmd_key = 'add_acl_rule'
#    command = conf.acl_commands[cmd_key].format(acl_chain=conf.nf_chain_prefix+name,src_ipset=name+conf.client_ipset_suffix)
#    fatal_error = run_command(stage, cmd_key, command.split())
#    if fatal_error: return 1
    
    cmd_key = 'chk_fwd_acl_c2s'
    command = conf.acl_commands[cmd_key].format(src_ipset=name+conf.client_ipset_suffix,dst_ipset=name+conf.dest_ipset_suffix)
    no_such_rule = run_command(stage, cmd_key, command.split())
    if no_such_rule: 
        cmd_key = 'add_fwd_acl_c2s'
        command = conf.acl_commands[cmd_key].format(src_ipset=name+conf.client_ipset_suffix,dst_ipset=name+conf.dest_ipset_suffix)
        fatal_error = run_command(stage, cmd_key, command.split())
        if fatal_error: return 1
    else:    
        return 1

    cmd_key = 'chk_fwd_acl_s2c'
    command = conf.acl_commands[cmd_key].format(src_ipset=name+conf.client_ipset_suffix,dst_ipset=name+conf.dest_ipset_suffix)
    no_such_rule = run_command(stage, cmd_key, command.split())
    if no_such_rule:
        cmd_key = 'add_fwd_acl_s2c'
        command = conf.acl_commands[cmd_key].format(src_ipset=name+conf.client_ipset_suffix,dst_ipset=name+conf.dest_ipset_suffix)
        fatal_error = run_command(stage, cmd_key, command.split())
        if fatal_error: return 1
    else:
        return 1

    return 0

def masquerading(cmd):
    stage = cmd + '_masquerade'
    '''
    cmd_list = [
        cmd + '_masq_ipset',
        cmd + '_intranet_ipset',
        cmd + '_masq_chain',
        cmd + '_fwd_advanced',
        cmd + '_masq_acl',
        cmd + '_masq_rule',
    ]
    if cmd == 'delete': cmd_list.reverse()
    '''
    if cmd == 'delete':
        cmd_list = conf.masq_uninit
    else:
        cmd_list = conf.masq_init
    for command in cmd_list:
        fatal_error = run_command(stage, cmd, command.split())
        if fatal_error: return 1



def update_user_auth(user_ip, common_name):
    stage = 'update_user_auth'
    logging.info('{}: User with common name: {} reconnected with IP address: {}'.format(stage, common_name, user_ip))
    return authorize_user(user_ip, common_name)

def deauth_user(user_ip):
    stage = 'deauth_user'
    ipset_groups = get_ovpn_group_list(conf.ipset_group)
    if len(ipset_groups) == 0:
        logging.error('{}: Can''t find IP Sets for current LDAP main group: {} '.format(stage, conf.ipset_group))
        return 1
    for acl_group in ipset_groups:
        group_dn = acl_group.entry_dn
        acl_basename = acl_group.entry_attributes_as_dict[conf.ipset_name_attr][0].replace(' ','_')
        client_ipset_name = acl_basename + conf.client_ipset_suffix
        cmd_key = 'check_ip'
        command = conf.acl_commands[cmd_key].format(src_set=client_ipset_name, user_ip=user_ip)
        no_ip_inset = run_command(stage, cmd_key, command.split())
        if no_ip_inset:
            logging.info('{}: No IP address: {} in set: {}'.format(stage, user_ip, client_ipset_name))
        else:
            cmd_key = 'del_ip'
            command = conf.acl_commands[cmd_key].format(src_set=client_ipset_name, user_ip=user_ip)
            run_command(stage, cmd_key, command.split())
            logging.info('{}: IP address: {} removed from set: {}'.format(stage, user_ip, client_ipset_name))    

    # Данному IP мог быть разрешён MASQUERADE, поэтому нужно это исправить.
    cmd_key = 'del_ip'
    command = conf.acl_commands[cmd_key].format(src_set='masquerade_allowed', user_ip=user_ip)
    run_command(stage, cmd_key, command.split())
 
    return 0

#
# Main function
#

def main():
    if len(sys.argv) < 3:
            logging.warning('USAGE: {} <add|update|delete>> <ip address> [common_name]'.format(sys.argv[0]))
            return False
    operation = sys.argv[1]
    logging.info('learn-script called with "{}" parameter.'.format(operation))
    if operation == 'add':
        ip_addr = sys.argv[2]
        common_name = sys.argv[3]
        return authorize_user(ip_addr, common_name)
    elif operation == 'update':
        ip_addr = sys.argv[2]
        common_name = sys.argv[3]
        return update_user_auth(ip_addr, common_name)
    elif operation == 'delete':
        ip_addr = sys.argv[2]
        return deauth_user(ip_addr)
    elif operation == 'masq':
        command = sys.argv[2]
        if command in ['create', 'delete']:
            return masquerading(command)
        else:
            logging.warning('Operation "{}" supports only "create" and "delete" parameters'.format(operation))
    else:
        logging.warning('Operation "{}" not supported! Use: "add", "update" or "delete"'.format(operation))
    return True

if __name__ == "__main__":
    exit_code = main()
    sys.exit(exit_code)
    logging.info('learn_script call completed!')



