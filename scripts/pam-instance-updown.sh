#!/bin/bash
LOGFILE=/var/log/ovpn-debug.log 
DATE=`/bin/date` 
IPT='/sbin/iptables'
IPS='/sbin/ipset'
IPSET_NAME='PAM-USERS'
IPSET_GIST='GIST'
LOCAL_NETS=(10.0.0.0/8 172.16.0.0/12 192.168.0.0/16 85.233.64.0/20 178.207.176.0/21)
IP_ADDR='10.8.55.64/26'
IPT_CHAIN_NAME='MASQ-PAM-USERS'

echo "**** $DATE **** pam-tcp-instance-up/BEGIN ****" >> $LOGFILE
echo $DATE $1 $2 $3 >> $LOGFILE

create_nf_entrys() {
	echo 'Initializing masquerade evironment for PAM VPN users...'
	$IPS create -exist $IPSET_NAME hash:net family inet hashsize 1024 maxelem 65536 comment
	$IPS add -exist $IPSET_NAME $IP_ADDR
	$IPT -A FORWARD -m set --match-set $IPSET_NAME src -j ACCEPT > /dev/null 2>&1
	$IPT -A FORWARD -m set --match-set $IPSET_NAME dst -j ACCEPT > /dev/null 2>&1
	$IPS create -exist $IPSET_GIST hash:net family inet hashsize 1024 maxelem 65536 comment
	for ip in ${LOCAL_NETS[*]}; do 
		$IPS add -exist $IPSET_GIST $ip
	done
	$IPT -t nat -N $IPT_CHAIN_NAME > /dev/null 2>&1
	$IPT -t nat -A $IPT_CHAIN_NAME -m set ! --match-set $IPSET_GIST dst -j MASQUERADE > /dev/null 2>&1
	$IPT -t nat -A POSTROUTING -m set --match-set $IPSET_NAME src -j $IPT_CHAIN_NAME > /dev/null 2>&1
}

remove_nf_entrys() {
        echo "Cleaning everything needed..."
	$IPT -t nat -D POSTROUTING -m set --match-set $IPSET_NAME src -j $IPT_CHAIN_NAME > /dev/null 2>&1
	$IPT -t nat -F $IPT_CHAIN_NAME > /dev/null 2>&1
	$IPT -t nat -X $IPT_CHAIN_NAME > /dev/null 2>&1
	$IPT -D FORWARD -m set --match-set $IPSET_NAME src -j ACCEPT > /dev/null 2>&1
	$IPT -D FORWARD -m set --match-set $IPSET_NAME dst -j ACCEPT > /dev/null 2>&1
	$IPS destroy -exist $IPSET_NAME > /dev/null 2>&1
	$IPS destroy -exist $IPSET_GIST > /dev/null 2>&1
}


case "$1" in
	down)
                remove_nf_entrys
                ;;

        up)
                remove_nf_entrys
                create_nf_entrys
		;;
	*)
		echo "Usage: $0 <up|down>"
esac

# pam-tcp-instance script environment dump
/usr/bin/env >> $LOGFILE
echo "**** $DATE **** pam-tcp-instance-up/END ****" >> $LOGFILE

exit 0
