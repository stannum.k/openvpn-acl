#!/bin/bash

LOGFILE=/var/log/ovpn-debug.log 
DATE=`/bin/date` 
echo "**** $DATE **** client-connect/BEGIN ****" >> $LOGFILE
echo $DATE $1 $2 $3 "client-connect: trusted_ip:" $trusted_ip ", ifconfig_pool_remote_ip: " $ifconfig_pool_remote_ip ", client dynamic config file: " $1 >> $LOGFILE

# client-connect script environment dump
/usr/bin/env >> $LOGFILE
echo "**** $DATE **** client-connect/END ****" >> $LOGFILE

