#!/bin/bash

LOGFILE=/var/log/ovpn-debug.log 
DATE=`/bin/date` 
echo "**** $DATE **** client-disconnect/BEGIN ****" >> $LOGFILE
echo $DATE $1 $2 $3 "client-disconnect: trusted_ip:" $trusted_ip "ifconfig_pool_remote_ip: " $ifconfig_pool_remote_ip >> $LOGFILE

# client-disconnect script environment dump
/usr/bin/env >> $LOGFILE
echo "**** $DATE **** client-disconnect/END ****" >> $LOGFILE

